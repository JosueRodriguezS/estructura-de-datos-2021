#include <stdio.h> //Libreria para utilizar el printf
#include <stdlib.h> //Libreria para utilizar calloc and realloc
#include <string.h>

/*La funcion eliminar_c no va a recibir ningun parametro. Esta se va a
 * recibir a traves de un scanf el parametro que el usuario ingrese. 
 * Luego este va a retirar las c y retornarlo.*/
char* eliminar_c(){
	
	long int lenght = 0; //va almacenar el largo del texto
	char* word = NULL; //va almacenar el texto
	int i = 1; //variable del indice
	long int new_lenght = 0; //guarda la nueva posicion de un caracter
	char letter = 'c'; //constante que contiene el caracter c
	
	printf("Ingrese el texto del cual desea elimanar el caracter c:");
	
	scanf("%c", word);
	
	lenght = sizeof(word) / sizeof(word[0]);
	
	char* new_word = calloc(lenght, sizeof(char));
		
	while(word[i] != '\0'){
		if(word[i] != letter){
			new_word[new_lenght] = word[i];
			new_lenght++;
			i++;
			}
		else{
			i++;
			}
			}
		
		printf("La palabra sin c es equivalente a %s", new_word);
			
		return (0);
}

/*La funcion invertir invierte el texto que es ingresado por el usuario
 * a traves del scanf.*/
char* invertir(){
	
	long int lenght = 0; //va almacenar el largo del texto
	long int i = 0; //Variable del indice
	char* text = NULL; //va almacenar el texto
	
	printf("Ingrese el texto que desea invertir:");
	
	scanf("%c", text);
	
	lenght = sizeof(text) / sizeof(text[0]);
	
	char* new_text = calloc(lenght, sizeof(char));
	
	while(text[i] != '\0'){
		lenght--;
		new_text[i] = text[lenght];
	}
	
	printf("La palabra %s revertida es %s", text, new_text);
	
	return (0);
}

/*La funcion heap lee y escribe un literal en la memoria heap a traves
 * de un for*/
char* heap(){
	
	printf("Ingrese el texto que desea:");
	
	int i = 0; //variable que contiene el indice
	char* text = NULL;
	
	scanf("%c", text);
	
	char* text_heap = calloc(strlen(text), sizeof(text));
	
	while (text != '\0'){
		text_heap[i] = text[i];
		i++;
	}
	printf("se a guardado la palabra %s en el heap", text_heap);
	return (0);
	}

int main(){
	
	//eliminar_c();
	
	//invertir();
	
	heap();
	
	}
