#include <stdio.h> //Libreria para utilizar el printf
#include <stdlib.h> //Libreria para utilizar div_t

/* La funcion ver_impar_par recibe como parametro un int (numero)
 * y va a retornar si el mismo es par o impar*/
int par_impar(int numero){
	
	/*Se crea una variable div_t (esto se refiere al valor integral de
	 * una division con su conciente almacenador en quot y su residuo
	 * en rem*/
	div_t division = div(numero, 2); 
	
	/*La condicional determinara que si el residuo de division es 0 es 
	 * par y si no es asi impar*/
	if (division.rem == 0){
		printf( "El numero %d es par.", numero);
	}
	else{
		printf( "El numero %d es impar.", numero);
	}
	
	return 0;

}

/*Esta funcion auxiliar se encarga de realizar la matematica para
 * encontrar a que caracter corresponde cierto numero en la secuencia
 * de fibonacci, recibe como parametro un int*/
int fibonacci_aux(int numero){
	
	if (numero <= 1){
		
		return numero;	
		
	}
	else{
		
		return fibonacci_aux(numero - 1) + fibonacci_aux(numero - 2);
		
	}
}

/*Esta funcion utiliza recibe como parametro un int que utiliza en la 
 * funcion auxiliar para luego imprimirlo en la interfaz*/
int fibonacci(int numero){
	
	int fibonacci_num = fibonacci_aux(numero);
	
	printf("El caracter %d en la secuancia de fibonacci es equivalente a %d", numero, fibonacci_num);

	return 0;
}

/*La funcion factorial auxiliar viene a realizar el proceso matematico 
 * necesario para obtener el factorial del parametro int (numero)*/
int factorial_aux(int numero){
	
	int num_factorial = 1;
	
	for (int i = numero; i > 1; i--){
		
		num_factorial = num_factorial * i;
		
	}
	
	return num_factorial;
}

/*La funcion factorial permite distinguir si el parametro numero es 
 * igual a 1 o 0, esto para retornar 1 o en todo caso retornar el 
 * resultado del proceso matematico necesario*/	
int factorial(int numero){
	
	if ((numero == 0 || numero == 1)){
		
		printf("El factorial de %d corresponde a 1", numero);
		
		return 0;
		
		}
	else{
		
		printf("El factorial de %d corresponde a %d", numero, factorial_aux(numero));
		
		return 0;
	
	}
}

/*La funcion length_for permite obtener el largo del parametro int 
 * numero a traves del uso del for*/
int length_for(int number){
	
	int num = number;
	int length = 0;
	
	for(int i = 1; i == 0; i++){
		
		if (num <= 1){
			break;
			}
		else {
			num = num/10;
			length = length + 1;
			}	
		}
	
	printf("El numero tiene un largo de %d", length);
	
	return 0;

}

/*La funcion length_while permite obtener el largo del parametro int 
 * numero a traves del uso del while*/
int length_while(int number){
	
	int contador = 0;
	
	 while(number != 0)  
   {  
       number = number/10;  
       
       contador++;  
   }
   printf("El largo del numero es %d", contador);
   
   return 0;
}

/*La funcion sumatorioa_for utiliza el loop de tipo for para realizar 
 * una sumatoria de n numero, que corresponderia al parametro numero*/
int sumatoria_for(int number){
	
	int suma = 1;
	
	for(int i = number; i == 1; i--){
		suma = suma + i;
		}
	
	printf("La suma da un total de %d", suma);
	
	return 0;
		
	}

/*La funcion sumatorioa_while utiliza el loop de tipo while para realizar 
 * una sumatoria de n numero, que corresponderia al parametro numero*/
int sumatoria_while(int number){
	int flag = number;
	int suma = 1;
	
	while(flag != 1){
		suma = suma + flag;
		flag--;
	}
	
	printf("La suma da un total de %d", suma);
	
	return 0;
}

/* La funcion alrevez toma un parametro int numero y por medio de un 
 * while esta toma de caracter en caracter para guardarlo en la variable
 * inverso y retornar el numero invertido*/
int alrevez(int numero){
	
	int num = numero;
	int residuo = 0;
	int inverso = 0;
	
	while(num != 0){
		
		residuo = num % 10;
		
		num = num / 10;
		
		inverso = inverso * 10 + residuo;
		
		}
		
		return inverso;
}

/*La funcion palindromo recibe un parametro int y por medio de la 
 * funcion alrevez compara si el numero al derecho y al revez son
 * exactamente iguales, para asi retornar un mensaje donde indique si 
 * lo es o no*/
int palindromo(int number){
	int num = number;
	int mirror_num = alrevez(number);
	
	if (num == mirror_num){
		
		printf("El numero %d si es palindromo", number);
		}
	else{
		printf("El numero %d no es palindromo", number);
		}
	return 0;
}

/*La funcion main al igual que java es donde se anade lo que el .exe
 * o lo compilable va a ser ejecutado*/		
int main(int argc, char** argv){
	
	//par_impar(25);
	
	//fibonacci (6);
	
	//factorial(5);
	
	//length_for(456);
	
	//length_while(456);
	
	//sumatoria_for(10);
	
	//sumatoria_while(10);
	
	//int n = 1002;
	
	//printf("El inverso de %d es %d", n, alrevez(1002));
	
	palindromo(1001);
	
	palindromo(1002);
	

	return 0;
	
}
